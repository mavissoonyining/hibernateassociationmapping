package org.dxc.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="answer")
public class Answer {

		@Id
		@Column(unique = true, nullable = false)
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		
		private int id;
		private String answerName;
		private String postedBy;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getAnswername() {
			return answerName;
		}
		public void setAnswername(String answername) {
			this.answerName = answername;
		}
		public String getPostedBy() {
			return postedBy;
		}
		public void setPostedBy(String postedBy) {
			this.postedBy = postedBy;
		}
		
}